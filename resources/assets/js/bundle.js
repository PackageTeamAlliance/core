(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Vue.component('content-table', {
  template: require('./templates/content/content.html'),
  props: ['data_uri', 'create_uri'],
  data: function data() {
    return {
      columns: [],
      contents: [],
      data_uri: '',
      filterKey: '',
      create_uri: ''
    };
  },
  methods: {
    'delete': function _delete(e) {
      e.preventDefault();
      var url = $(e.target).data('url');
      console.log(bootbox);
      bootbox.confirm("Are you sure you want to delete this role? This action can not be undone.", function (result) {
        if (result) {
          location.href = url;
        }
      });
    }
  },
  created: function created() {
    // GET request
    this.$http.get(this.data_uri, function (data, status, request) {

      // set data on vm
      this.$set('columns', data.columns);
      this.$set('contents', data.content);

      this.$$.progress.style.display = 'none';
      this.$$.table.style.display = 'table';
    }).error(function (data, status, request) {
      // handle error
    });
  }
});

new Vue({
  el: "#content-table"
});

},{"./templates/content/content.html":3}],2:[function(require,module,exports){
'use strict';

Vue.component('pages-table', {
  template: require('./templates/pages/pages.html'),
  props: ['data_uri', 'create_uri'],
  data: function data() {
    return {
      columns: [],
      contents: [],
      data_uri: '',
      filterKey: '',
      create_uri: ''
    };
  },
  methods: {
    'delete': function _delete(e) {
      e.preventDefault();
      var url = $(e.target).data('url');
      console.log(bootbox);
      bootbox.confirm("Are you sure you want to delete this role? This action can not be undone.", function (result) {
        if (result) {
          location.href = url;
        }
      });
    }
  },
  created: function created() {
    // GET request
    this.$http.get(this.data_uri, function (data, status, request) {

      // set data on vm
      this.$set('columns', data.columns);
      this.$set('pages', data.content);

      this.$$.progress.style.display = 'none';
      this.$$.table.style.display = 'table';
    }).error(function (data, status, request) {
      // handle error
    });
  }
});

new Vue({
  el: "#pages-table"
});

},{"./templates/pages/pages.html":4}],3:[function(require,module,exports){
module.exports = '\n<div class="row filter-block">\n	<div class="pull-left">\n		<a href="{{create_uri}}" class="btn-flat success">Add New Content Item</a>\n	</div>\n    <div class="pull-right">\n        <input type="text" class="search table-search" placeholder="" v-on="keydown : search" v-model="searchKey"/>\n    </div>\n</div>\n\n<br>\n<div class="row">\n\n	<div class="progress" v-el="progress">\n	  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">\n	    <span class="sr-only">45% Complete</span>\n	  </div>\n	</div>\n	\n	<div class="table-responsive">\n		<table class="table table-hover" data-route="" v-el="table" style="display:none;">\n			<thead>\n				<tr>\n					<th v-repeat="columns">{{$value}}</th>\n				</tr>\n			</thead>\n			<tbody>\n\n				<tr v-repeat="content: contents  | filterBy searchKey">\n					<td>{{content.id}}</td>\n					<td>{{content.name}}</td>\n					<td><code>{{content.slug}}</code></td>\n					<td>{{content.type}}</td>\n					<td><span class="label label-primary">{{content.html}}</span></td>\n					<td>\n						<span v-repeat="translation : content.translation">\n							<a href="{{translation.url}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i>{{translation.locale}}</a>\n						</span>\n					</td>\n\n					<td>\n				        <a href="{{content.copy}}" class="btn btn-icon btn-circle btn-primary" data-click="panel-collapse"><i class="fa fa-refresh"></i></a>\n				        <a href="{{content.edit}}" class="btn btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-pencil"></i></a>\n						<a href="{{content.translate}}" class="btn btn-icon btn-circle btn-success"><i class="fa fa-flag"></i></a>\n				        <button class="btn-delete btn btn-icon btn-circle btn-danger" data-click="panel-remove" v-on="click: delete" data-url="{{content.delete}}">Delete</button>\n					</td>\n					\n				</tr>\n				\n			</tbody>\n		</table>\n	</div>\n\n	<div class="row ctrls">\n	  	\n	</div>\n\n</div>';
},{}],4:[function(require,module,exports){
module.exports = '<div class="row filter-block">\n	<div class="pull-left">\n		<a href="{{create_uri}}" class="btn-flat success">Add New Page</a>\n	</div>\n    <div class="pull-right">\n        <input type="text" class="search table-search" placeholder="" v-on="keydown : search" v-model="searchKey"/>\n    </div>\n</div>\n\n<br>\n<div class="row">\n\n<div class="progress" v-el="progress">\n  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">\n    <span class="sr-only">45% Complete</span>\n  </div>\n</div>\n<div class="table-responsive">\n	<table class="table">\n		<thead>\n			<tr>\n				<th v-repeat="columns">{{$value}}</th>\n			</tr>\n		</thead>\n		<tbody>\n			\n			<tr v-repeat="page: pages | filterBy searchKey">\n				<td>{{page.id}}</td>\n				<td>{{page.name}}</td>\n				<td><code>{{page.uri}}</code></td>\n				<td><span class="label label-success">{{page.type}}</span></td>\n				<td><code>{{page.section}}}</code></td>\n				\n				<td><span class="label label-success">{{page.active}}</span>\n				<td>\n					<span v-repeat="translation : page.translation">\n						<a href="{{translation.url}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i>{{translation.locale}}</a>\n					</span>\n				\n				</td>\n\n				<td>\n                  	<a href="{{page.copy}}" class="btn btn-icon btn-primary" data-click="panel-collapse"><i class="fa fa-refresh"></i></a>\n		        	<a href="{{page.edit}}" class="btn btn-icon btn-warning" data-click="panel-collapse"><i class="fa fa-pencil"></i></a>\n					<a href="{{page.translate}}" class="btn btn-icon btn-success"><i class="fa fa-flag"></i></a>\n		        	<button class="btn-delete btn btn-icon btn-danger" data-click="panel-remove" v-on="click: delete" data-url="{{page.delete}}">Delete</button>\n  				</td>\n\n  				<td><a href="{{page.uri}}" class="btn btn-icon btn-success"><i class="fa fa-eye"></i></a></td>\n			</tr>\n		</tbody>\n	</table>\n</div>\n';
},{}]},{},[1,2]);
