Vue.component('content-table', {
	template: require('./templates/content/content.html'),
	props: ['data_uri', 'create_uri'],
	data: function () {
    	return {
      		columns: [],
      		contents: [],
      		data_uri: '',
    		filterKey : '',
      		create_uri: '',
    	}
  	},
	 methods : {
         delete: function (e){
         	e.preventDefault();
         	var url = $(e.target).data('url');
         	console.log(bootbox);
         	bootbox.confirm("Are you sure you want to delete this role? This action can not be undone.", function(result){
         		if(result)
				{
					location.href=url;
				}

         	});
         	
          }
      },
      created: function() {
            // GET request
            this.$http.get(this.data_uri, function (data, status, request) {

              // set data on vm
              this.$set('columns', data.columns);
              this.$set('contents', data.content);

              this.$$.progress.style.display = 'none';
              this.$$.table.style.display = 'table';

            }).error(function (data, status, request) {
              // handle error
          })
      }
});

new Vue({
	el: "#content-table"
});