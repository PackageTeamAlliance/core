@extends('frontend.layouts.master')
@section('content')
<div class="container">
     <div class="row row-same-height" id="main-boxes">
    	<div class="col-md-4">
        	<div class="home-box">
            	<a href="/all-courses" title="All Courses">
                	<img src="public/img/home/icon-home-apply.png" alt="">

                    <h3>Courses</h3>
                    <p>We offer a variety of courses to help you meet your educational goals. View Courses.</p>
                </a>
            </div>
        </div>
        
        <div class="col-md-4">
        	<div class="home-box">
            	<a href="/partner-colleges" title="Partner Colleges">
                	<img src="{{--Asset::getUrl('public/img/home/icon-home-visit.png')--}}" alt="">
                    <h3>Partner Colleges</h3>
                    <p>View a list of our regionally accredited partner universities.</p>
                </a>
            </div>
        </div>
        
        <div class="col-md-4">
        	<div class="home-box">
            	<a href="/how-it-works" title="How it Works">
                	<img src="{{--Asset::getUrl('public/img/home/icon-home-visit.png')--}}" alt="">
                    <h3>How it Works</h3>
                    <p>Find out how Westcott Courses works with colleges to help you meet your educational goals.</p>
                </a>
            </div>
        </div>
    </div>
</div> <!-- end container-->

<div class="container">
    <div class="row">
        <div class="course-list-contaier">
			<h2>Top Courses<br /><small>Check back for new courses!</small></h2>
			<p class="">Below are our top selling courses!  Westcott Courses &amp; Omega Math strives to build high quality courses that meet the standards for regional accreditation. We believe educational excellence is accomplished when the class is challenging and provides the student with the resources they need to be successful in and out of the classroom. We are continuously working to create new courses with our accrediting universities, so please check back with us as we add more courses.</p>
                <p class="visible-xs"><a href="/all-courses" class="button_large">View all courses </a></p>
                <!-- Ajax Injects courses here -->
                <section id="course-list">
                    <div class="table-responsive table">
                        <table class="table table-hover course-list-table tablesorter">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Course Name</th>
                                <th>Course Department</th>
                                <!-- <th class="starts">Starts</th> -->
                                <!-- <th class="length">Credit Options</th> -->
                            </tr>
                            </thead>
                              <tbody>
                            @foreach($highlights as $course)
                                <tr>
                                    
                                    @foreach($departments as $department)
                                        @if($course->department_id == $department->id)                                        
                                            <th><i class="fa fa-fire"></i></th>
                                            <th class="course-title"><a href="{{route('frontend.wc.lms.courses.detail', [$course->alias]) }}" class="coursetitle" data-container="tbody" data-toggle="popover"  data-content='{{$course->description}}'>{{$course->name}}</a></th>
                                            <th class="course-category"><a href="{{route('frontend.wc.lms.courses.department', [$department->alias])}}">{{$department->name}}</a></th>
                                        @endif
                                    @endforeach
                                    
                                    
                                </tr>                                        
                            @endforeach
                            </tbody>
                        </table>
                    </div>
					<p class="text-center"><a href="{{route('frontend.wc.lms.courses.all')}}" class="button_large">View all courses </a></p>
                </section>


              @include('frontend.partials/courses-search-form')               
            </div>
        </div>
    </div><!-- end container-->

@stop


@section('scripts')
<script>

$('.coursetitle').popover({
	trigger: 'hover',
	html: true,
    placement: 'right'
});

$('#is_lab').selectize({
create: false,
sortField: {
    field: 'text',
    direction: 'asc'
},
		dropdownParent: 'body'
	});

	 $('#department').selectize({
    create: false,
    sortField: {
        field: 'text',
        direction: 'asc'
    },
    dropdownParent: 'body'
});

	
</script>
@stop