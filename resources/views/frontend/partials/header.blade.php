<?php $user = 'guest'; $admin = false; ?>
<header>
	<div class="container headcontainer">
   	  <div class="row">
    	<div class="col-md-5" id="logo">
            <a href="{{{url('/')}}}">
                <img src="{{url('images/global/brand/logo.png')}}" alt="'s header logo" title="header logo" id="headerlogo">
            </a>
            <h1 id="sitetitle">@content('site-name')</h1>
            <p id="slogan">Publisher of <strong>Omega Math<sup>TM</sup></strong> Courses</p></div>
        <div class="col-md-7">        
            <div id="phone2" class="hidden-xs"><strong>(805) 489 2831 </strong>Advising Center</div>
            <div id="menu-top">
            	<ul><?php if($admin){ $route = 'admin/index'; $title = "Dashboard";}else { $route = 'users/index'; $title = "My Account";} ?>
                    <li><?php if($user != 'guest'){echo 'Hello '.$user->first_name.',<br /><a href="/'.$route.'" class="button_medium"  title="'.$title.'">'.$title.'</a> '; }?></li>
                	<!-- <li><a href="/index" title="Home">Home</a> | </li> -->
                    <!-- <li><a href="/news-events" title="News and Events">News &amp; Events</a> | </li> -->
                    <?php if($user == 'guest'){ echo '<li><a href="http://my.westcottcourses.com/scripts/westcott-login.php" target="_blank" class="button_medium" title="opens new window">Login</a></li>'; }else { echo '<li><a href="/admin/logout" class="button_medium" ">Logout</a></li>';  } ?>
                </ul>
            </div>
            <!-- <div id="catchphrase"><em>We have been online since 1996 working with <br/>California Polytechnic State University -  San Luis Obispo</em></div> -->
        </div><!-- End span8-->
        </div><!-- End row-->
    </div><!-- End container-->
</header><!-- End Header-->