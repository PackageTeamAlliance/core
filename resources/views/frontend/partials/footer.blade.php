<footer>
  <div class="container">
  	<div class="row">
    	<div class="col-md-4" id="brand-footer">
   	    	<p><a href="/"><img src="{{url('images/global/brand/logo.png')}}" title="" alt="'s footer logo" id="footerlogo"></a></p>
            <p>Copyright © <?php echo date("Y"); ?></p>
            <div class="twitter"><a href="https://twitter.com/"  target="_blank" title="opens new window">Follow on Twitter</a></div>
			<div class="fb"><a href="https://www.facebook.com/" target="_blank" title="opens new window">Follow on Facebook</a></div>
        </div>
        <div class="col-md-4" id="contacts-footer">
        	<h4>Contacts</h4>
            <ul>
            	<li><i class="fa fa-home"></i> , PO Box 835 Pismo Beach, CA. 93448</li>
            	<li><i class="fa fa-phone"></i> Telephone: (805) 489-2831</li>                
                <li><i class="fa fa-envelope"></i> Email: <a href="mailto:support@.com">Support</a></li>
            </ul>
            <hr>
        	<h4>Newsletter</h4>
            <p>To find out about new classes, updates, and other exciting news sign up for our newsletter below!</p>
            
            <div id="message-newsletter"></div>
                <form method="post"  action="/newsletter" name="newsletter" id="newsletter" class="form-inline">
                    <div class="input-group">
                    
                      <input class="form-control" name="email_newsletter" id="email_newsletter" type="email" value="" placeholder="Your Email" >
                      <span class="input-group-btn">
                
                        <button  id="submit-newsletter" class="btn btn-secondary btn-lg"> Subscribe</button>
                      </span>
                    </div><!-- /input-group -->

                </form>
        	</div>
        <div class="col-md-4" id="quick-links">
        	<h4>Quick links</h4>
            <ul>
            	<li><a href="/how-it-works" title="How it works" >How it works</a></li>
                <li><a href="/partner-colleges" title="Partner Colleges">Partner Colleges</a></li>
            	<li><a href="/all-courses" title="All Courses">All Courses</a></li>
                <li><a href="/about-us" title="About Us">About Us</a></li>
                <li><a href="/transfer-the-credit" title="Transfering the Credit">Transferring the Credit</a></li>
                <li><a href="https://my.westcottcourses.com/registration/placementStart.php" title="Placement Test">Placement Test</a></li>

            </ul>
            <hr>
            <ul>
                <li><a href="/counselors" title="College Counselors">College Counselors</a></li>
                <li><a href="/eop" title="EOP &amp; Other Programs">EOP &amp; Other Programs</a></li>
                <li><a href="https://publishing.westcottcourses.com/" title="Learn about our publishing division opens new window" target="_blank">Learn about our licensing</a></li>
                <li><a href="/admissions" title="Admissions">Admissions</a></li>
                <li><a href="/math-department" title="Math &amp; English Department">Math Department</a></li>
                <li><a href="/english-department" title="Math &amp; English Department">English Department</a></li>
            </ul>
        </div>
        
    </div>
  </div>
  </footer><!-- End footer-->
<div id="toTop">Back to Top</div>
                