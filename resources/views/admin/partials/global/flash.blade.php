@if (count($errors) > 0)
<div class="row">
	<div class="alert alert-danger fade in">
	<strong>{{ trans('pta/content::message.general.error') }}</strong>
		<ul>
	        @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	        @endforeach
	    </ul>
	</div>
</div>
@endif

@if (Session::has('flash_notification.message'))
<div class="row">
    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

        {{ Session::get('flash_notification.message') }}
    </div>
</div>
@endif