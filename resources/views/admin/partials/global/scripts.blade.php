<script type="text/javascript">
	Vue.http.headers.common["X-CSRF-TOKEN"]= '{{{csrf_token()}}}';
    $("#dashboard-menu .dropdown-toggle").click(function (e) {
            e.preventDefault();
            var $item = $(this).parent();
            $item.toggleClass("active");
            if ($item.hasClass("active")) {
                $item.find(".submenu").slideDown("fast");
            } else {
                $item.find(".submenu").slideUp("fast");
            }
        });
      // mobile side-menu slide toggler
      var $menu = $("#sidebar-nav");
      $("body").click(function () {
        if ($(this).hasClass("menu")) {
          $(this).removeClass("menu");
        }
      });
      $menu.click(function(e) {
        e.stopPropagation();
      });
      $("#menu-toggler").click(function (e) {
        e.stopPropagation();
        $("body").toggleClass("menu");
      });
      $(window).resize(function() { 
        $(this).width() > 769 && $("body.menu").removeClass("menu")
      })
      $('li.active').parent().addClass('active');
      $('li.active').parent().parent().addClass('active');
</script>