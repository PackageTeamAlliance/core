<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield('page-title')</title>

    <link href="{{{url('assets/css/admin/admin-all.css')}}}" rel="stylesheet">
    {{-- <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet"> --}}

</head>

<body class="fixed-sidebar no-skin-config full-height-layout">
    <!-- #wrapper -->
    <div id="wrapper">

    @include('admin.partials.nav')
    
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            
        </div>
            <ul class="nav navbar-top-links navbar-right">

                <li>
                    <a href="{{{ route('app.logout') }}}">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>@yield('page-title')</h2>
                    @yield('breadcrumb')
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content">
                
                   @yield('content')
                
            </div>
            <div class="footer">
                <div class="pull-right">
                    <strong>Copyright</strong> Example Company &copy; 2014-2015
                </div>
                
            </div>

        </div>
    </div>
    <!-- /#wrapper -->

    <script src="{{{url('assets/js/admin/admin.js')}}}"></script>

    @yield('scripts')
    </body>
</body>