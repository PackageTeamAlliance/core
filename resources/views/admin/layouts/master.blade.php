<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <title>@yield('page-title')</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <meta content="{{csrf_token()}}" name="csrf-token" id="csrf-token">

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link rel="stylesheet" href="{{ url('stylesheets/admin/admin-all.css') }}">
    <!-- ================== END BASE CSS STYLE ================== -->

</head>
<body>

@include('admin.partials.global.navbar')

@include('admin.partials.global.sidebar_nav')


{{-- main container --}}
<div class="content" style="padding:20px 30px;" @yield('vue-id')>
    <div class="container">
        @include('admin.partials.global.flash')
    </div>
    {{-- begin page-header --}}
    <div class="col-md-12">
        <h1 class="page-header">
            <small>@yield('page-title')</small>
        </h1>
    </div>
    {{--  end page-header --}}

    {{-- pad-wrapper --}}
    <div id="pad-wrapper" class="form-page">


        <div class="row form-wrapper">
            {{--  --}}
            <div class="col-md-8 column">
                @yield('content')
            </div>
            {{-- / main coluln --}}

            {{-- Side bar --}}
            <div class="col-md-4 column pull-right">
                @yield('sidebar')
            </div>
            {{--/ Side bar --}}
        </div>
    </div>
    {{-- / pad-wrapper --}}

</div>
{{--  / main container --}}

<script src="{{ url('javascript/admin/admin-all.js') }}"></script>
@include('admin.partials.global.scripts')

@yield('scripts')

</body>
</html>
