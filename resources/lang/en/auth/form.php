<?php

return [

	'first_name'                  => 'First Name',
	'first_name_help'             => 'Type your first name.',
	'first_name_error'            => 'A first name is required.',
	'last_name'                   => 'Last Name',
	'last_name_help'              => 'Type your last name.',
	'last_name_error'             => 'A last name is required.',
	'username'                    => 'Username',
	'email'                       => 'Email',
	'email_placeholder'           => 'me@example.com',
	'email_help'                  => 'Type your email address.',
	'email_error'                 => 'A valid email address is required.',
	'password'                    => 'Password',
	'password_placeholder'        => '&#9679;&#9679;&#9679;&#9679;&#9679;',
	'password_help'               => 'Type your password.',
	'password_error'              => 'The password must be at least 6 characters.',
	'password_confirmation'       => 'Confirm Password',
	'password_confirmation_help'  => 'Confirm your password.',
	'password_confirmation_error' => 'Password does not match.',
	'required'                    => 'Required',

	'login' => [
		'legend'          => 'Sign in to get started',
		'legend_social'   => 'Sign in with',
		'remember-me'     => 'Remember me',
		'submit'          => 'Sign in',
		'forgot-password' => 'Password',
		'no_account'      => 'Don\'t have an Account?',
		'register'        => 'Register',
		'or_recover'      => 'or recover your',
	],

	'login-social' => [
		'legend'   => 'Social Logins',
	],

	'register' => [
		'legend'   => 'Sign up',
		'submit'   => 'Create an account',
		'disabled' => 'Registration is disabled!',
	],

	'profile' => [
		'legend'   => 'Profile',
		'submit'   => 'Save',
	],

	'forgot-password' => [
		'legend' => 'Forgot Password',
		'submit' => 'Forgot Password',
	],

	'reset-password' => [
		'legend'                     => 'Change your Password',
		'password'                   => 'New Password',
		'password_help'              => 'Type your new password.',
		'password_confirmation'      => 'Confirm Password',
		'password_confirmation_help' => 'Confirm your new password.',
		'submit'                     => 'Change Password',
	],

];
