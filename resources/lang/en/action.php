<?php
/**
 * 
 *
 * 
 *
 * 
 *
 * 
 * 
 *
 * 
 * 
 * 
 * 
 * 
 * 
 */

return [

	'add'       => 'Add',
	'create'    => 'Create',
	'remove'    => 'Remove',
	'cancel'    => 'Cancel',
	'delete'    => 'Delete',
	'edit'      => 'Edit',
	'update'    => 'Save Changes',
	'submit'    => 'Submit',
	'save'      => 'Save',
	'copy'      => 'Copy',
	'update'    => 'Update',
	'enable'    => 'Enable',
	'disable'   => 'Disable',
	'install'   => 'Install',
	'uninstall' => 'Uninstall',
	'open'      => 'Open',
	'close'     => 'Close',
	'collapse'  => 'Open/Close',
	'upload'    => 'Upload',
	'export'    => 'Export',
	'translate' => 'Translate',

	'bulk' => [
		'delete'  => 'Delete Selected',
		'enable'  => 'Enable Selected',
		'disable' => 'Disable Selected',
	],

];
