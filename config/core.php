<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default admin URI
    |--------------------------------------------------------------------------
    |
    | This is the optional core framework admin facing uri
    | All modules will use this as the base of 
    | their urls that are constructed
    |
    */

    'admin_uri' => 'dashboard/',

    /*
    |--------------------------------------------------------------------------
    | Default Login URI
    |--------------------------------------------------------------------------
    |
    | This is the optional core framework admin facing uri
    | All modules will use this as the base of 
    | their urls that are constructed
    |
    */

    'login_uri' => '/login',

    /*
    |--------------------------------------------------------------------------
    | Allow User Registration
    |--------------------------------------------------------------------------
    |
    | This variable will determin if a user is
    | allowed to register for the site this
    | will hide/show the nav item
    |
    */

    'allow_registration' => true,

    /*
    |--------------------------------------------------------------------------
    | User Registration Default Role
    |--------------------------------------------------------------------------
    |
    |  This will indicate which role the user will be added to
    |  when they register, this will be used when the event
    |  is raised during using creation.
    |
    */

    'registration_default_role' => '',


    /*
    |--------------------------------------------------------------------------
    | Default System redirect URL
    |--------------------------------------------------------------------------
    |
    | This variable allows us to use a common url schema to
    | consistently redirect users that try to access 
    | a url they shouldn't due to their role
    |
    */

    'redirect_url' => '/',

    /*
    |--------------------------------------------------------------------------
    | Default System admin role
    |--------------------------------------------------------------------------
    |
    | This variable allows us to use a common url schema to
    | consistently redirect users that try to access 
    | a url they shouldn't due to their role
    |
    */

    'admin_role' => 'admin',

    /*
    |--------------------------------------------------------------------------
    | Default System admin nav
    |--------------------------------------------------------------------------
    |
    | This variable allows us to use a common url schema to
    | consistently redirect users that try to access 
    | a url they shouldn't due to their role
    |
    */

    'admin_nav' => [

        'name'        => 'core-sidenav',
        'presenter'   => App\Presenters\SideBarNavPresenter::class,
        'module_name' => 'core',

    ],

    /*
    |--------------------------------------------------------------------------
    | Default System frontend nav
    |--------------------------------------------------------------------------
    |
    | This variable allows us to use a common url schema to
    | consistently redirect users that try to access 
    | a url they shouldn't due to their role
    |
    */

    'site_nav' => [

        'name'        => 'core-sitenav',
        'presenter'   => Pta\Menus\Presenters\Bootstrap\NavbarPresenter::class,
        'module_name' => 'core',

    ]



];
