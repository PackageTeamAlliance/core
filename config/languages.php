<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Language confi
    |--------------------------------------------------------------------------
    |
    | 
    | 
    | 
    | 
    |
    */
    'default' => [
        'name'       => 'English',
        'short_name' => 'en',
    ],
    'locale' =>[      
        [
            'name'       => 'French',
            'short_name' => 'fr',
        ],
        [
            'name'       => 'Spanish',
            'short_name' => 'sp',
        ],
    ]
];
