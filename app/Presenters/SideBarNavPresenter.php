<?php

namespace App\Presenters;

use Pta\Menus\Presenters\Presenter;

class SideBarNavPresenter extends Presenter
{
    /**
     * {@inheritdoc }
     */
    public function getOpenTagWrapper()
    {
        return  PHP_EOL .'<div id="sidebar-nav">' . PHP_EOL . '<ul class="nav" id="dashboard-menu">' . PHP_EOL;
    }

    /**
     * {@inheritdoc }
     */
    public function getCloseTagWrapper()
    {
        return  PHP_EOL . '</ul>' . PHP_EOL. '</div>'. PHP_EOL;
    }

    /**
     * {@inheritdoc }
     */
    public function getMenuWithoutDropdownWrapper($item)
    {
        // return '<li class="'.$this->getActiveState($item).' "><a href="'. $item->getUrl() .'">'.$item->getIcon().' '.$item->title.'</a></li>'. PHP_EOL;
        // 
        return '<li class="'.$this->getActiveState($item).' ">'
                .'<a href="'. $item->getUrl(). '">'. PHP_EOL
                    .$item->getIcon() .PHP_EOL
                    .'<span>'.$item->title.'</span>
                </a>
            </li>';
    }

    /**
     * {@inheritdoc }
     */
    public function getActiveState($item)
    {
        return \Request::is($item->getRequest()) ? 'active' : null;
    }

    /**
     * {@inheritdoc }
     */
    public function getDividerWrapper()
    {
        return '<li class="divider"></li>';
    }

    /**
     * {@inheritdoc }
     */
    public function getMenuWithDropDownWrapper($item)
    {
        return '<li '. 'class="'. $this->getActiveState($item). '"'. '>
                <a class="dropdown-toggle" href="#">'
                . $item->getIcon().
                '<span> '.$item->title.'</span>
                </a>
                <ul class="submenu '. $this->getActiveState($item). '">
                  '.$this->getChildMenuItems($item).'
                </ul>
              </li>' . PHP_EOL;
    }
}