<?php

namespace App\Http\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


trait AuthenticatesUsers
{
    use RedirectsUsers;

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {

        $this->validate($request, [
            'username' => 'required', 'password' => 'required',
        ]);

        $credentials = $this->getCredentials($request);

        if (Auth::attempt($credentials, $request->has('remember'))) {
            $user = Auth::user();

//            if(!$user->active){
//                \Flash::error('Your account has been deactivated, please contact support if you feel this was in error.');
//                Auth::logout();
//                return back();
//            }

            $userPermissions = $user->permissions;
            $roles = $user->roles;
            $roles->load('permissions');
            $permissions = [];
            $userRoles = [];

            foreach ($roles as $role) {
                $userRoles[] = $role->name;
                if(!$role->permissions->isEmpty()){
                    foreach($role->permissions as $permission)
                    $userPermissions[] =  $permission->name;
                }
            }


            if(!$userPermissions->isEmpty()){
                foreach ($userPermissions as $permission){
                    if(!in_array($permission, $permissions)){
                        $permissions[] = $permission;
                    }
                }
            }

            $request->session()->put('roles', $userRoles);
            $request->session()->put('permissions', $permissions);

            return redirect()->intended($this->redirectPath());
        }

        return redirect($this->loginPath())
            ->withInput($request->only('username', 'remember'))
            ->withErrors([
                'username' => $this->getFailedLoginMessage(),
            ]);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return $request->only('username', 'password');
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return 'These credentials do not match our records.';
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {

        Auth::logout();

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

    /**
     * Get the path to the login route.
     *
     * @return string
     */
    public function loginPath()
    {
        return property_exists($this, 'loginPath') ? $this->loginPath : '/auth/login';
    }
}
