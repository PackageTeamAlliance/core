<?php

namespace App\Http\Middleware;

use Closure;
use Artesaos\Defender\Middlewares\NeedsPermissionMiddleware as DefenderNeedsPermission;

class NeedsPermissionMiddleware extends DefenderNeedsPermission
{
    
    /**
     * @param \Illuminate\Http\Request $request
     * @param callable                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $permissions = null, $any = false) {
        if (is_null($permissions)) {
            $permissions = $this->getPermissions($request);
            $anyPermission = $this->getAny($request);
        } 
        else {
            $permissions = explode('|', $permissions);
            
            // Laravel 5.1 - Using parameters
            $anyPermission = $any;
        }
        
        if (is_null($this->user)) {
            return $this->forbiddenResponse();
        }
        
        if (is_array($permissions) and count($permissions) > 0) {
            $canResult = true;
            
            foreach ($permissions as $permission) {
                $canPermissions = $request->session()->get('permissions');
                
                if (is_array($canPermissions)) {
                    if (in_array($permission, $canPermissions)) {
                        $canPermission = true;
                    } 
                    else {
                        $canPermission = false;
                    }
                    
                    // Check if any permission is enough
                    if ($anyPermission and $canPermission) {
                        return $next($request);
                    }
                    
                    $canResult = $canResult & $canPermission;
                } 
                else {
                    return $this->forbiddenResponse();
                }
            }
            
            if (!$canResult) {
                return $this->forbiddenResponse();
            }
        }
        
        return $next($request);
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    private function getPermissions($request) {
        $routeActions = $this->getActions($request);
        
        $permissions = array_get($routeActions, 'can', []);
        
        return is_array($permissions) ? $permissions : (array)$permissions;
    }
    
    /**
     * Handles the forbidden response.
     *
     * @return mixed
     */
    protected function forbiddenResponse() {
        
        flash()->error('You do not have permissions to access this resource.');
        
        return redirect(config('core.redirect_url'));
    }
}
