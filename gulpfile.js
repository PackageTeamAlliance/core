var elixir = require('laravel-elixir');


elixir.config.cssOutput = 'public/stylesheets';

//complie to css
elixir(function (mix) {
  mix
  .sass([
    "/bootstrap/bootstrap.scss"
  ], 'resources/assets/css/bootstrap/bootstrap.css')

  .sass([
    "/frontend/frontend.scss"
  ], 'resources/assets/css/frontend/frontend.css')
  .sass([
    "/fontawesome/fontawesome.scss"
  ], 'resources/assets/css/fontawesome/fontawesome.css')

  .sass([
    "/admin/admin.scss"
  ], 'resources/assets/css/admin/admin.css');
});

elixir(function (mix) {
  mix.styles([
    'bootstrap/bootstrap.css',
    'bootstrap/bootstrap-overrides.css',
    'fontawesome/fontawesome.css',
    'admin/admin.css',
     'redector/css/redactor.css',
    '../../../node_modules/select2/dist/css/select2.min.css',
    '../../../node_modules/bootstrap-daterangepicker/daterangepicker.css',
    '../../../node_modules/dropzone/dist/dropzone.css',
    '../../../node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
    '../../../node_modules/cropper/dist/cropper.css',
    '../../../node_modules/selectize/dist/css/selectize.bootstrap3.css',
  ] , 'public/stylesheets/admin/admin-all.css')
  .styles([
    'bootstrap/bootstrap.css',
    'frontend/frontend.css'
  ] , 'public/stylesheets/frontend/frontend-all.css');
});

elixir(function (mix) {
  mix.copy('node_modules/fontawesome/fonts', 'public/fonts/fontawesome/');
})


elixir(function(mix) {
    mix
    .scripts([
      '../../../node_modules/jquery/dist/jquery.min.js',
      '../../../node_modules/vue/dist/vue.min.js',
      '../../../node_modules/vue-resource/dist/vue-resource.min.js',
      '../../../node_modules/selectize/dist/js/standalone/selectize.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/alert.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/affix.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/button.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/carousel.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/collapse.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/collapse.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/modal.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/tab.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/transition.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/popover.js',
      
      
    ], 'public/javascript/frontend/frontend-all.js')
    .browserify([
      'components/content-datatable.js',
      'components/pages-datatable.js'
    ], './resources/assets/js/bundle.js')
    .scripts([
      '../../../node_modules/jquery/dist/jquery.min.js',
      '../../../node_modules/select2/dist/js/select2.full.min.js',
      '../../../node_modules/moment/min/moment.min.js',
      '../../../node_modules/bootstrap-daterangepicker/daterangepicker.js',
      '../../../node_modules/vue/dist/vue.min.js',
      '../../../node_modules/vue-resource/dist/vue-resource.min.js',
      '../../../node_modules/dropzone/dist/dropzone.js',
      '../../../node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',
      '../../../node_modules/cropper/dist/cropper.js',
      '../../../node_modules/selectize/dist/js/standalone/selectize.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/alert.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/affix.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/button.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/carousel.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/collapse.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/collapse.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/modal.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/tab.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/transition.js',
      '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/popover.js',
      '../../../node_modules/bootbox/bootbox.js',
      // '../../../node_modules/isotope/dist/isotope.pkgd.min.js',
      // '../../../',
      'redector/js/redactor.min.js',
      'slugify.js',
      'nestable.js',
      'editmode.js',
      'bundle.js',
      


      ], 'public/javascript/admin/admin-all.js');
});