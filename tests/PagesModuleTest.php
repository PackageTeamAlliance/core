<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class PagesModuleTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_admin_user_can_access_create_page()
    {
        $user = $this->create_user();

        \Auth::loginUsingId($user->id);

        $create_page_url = route('pages.dashboard.create');

        $this->visit($create_page_url)->seePageIs($create_page_url);
    }

    public function test_logged_in_user_cant_access_pages()
    {
        $user = $this->create_user(false);

        \Auth::loginUsingId($user->id);

        $create_page_url = route('pages.dashboard.create');

        $this->visit($create_page_url)->see('You are not authorized to access this section of the website');
    }

    public function test_logged_out_user_cant_access_pages()
    {
        $create_page_url = route('pages.dashboard.create');

        $this->visit($create_page_url)->see('You are not authorized to access this section of the website');
    }

    public function test_admin_user_can_create_page()
    {
        $user = $this->create_user();

        \Auth::loginUsingId($user->id);

        $create_page_url = route('pages.dashboard.create');

        $this->visit($create_page_url)
            ->type('Test Page', 'name')
            ->type('test-page', 'uri')
            ->select('database', 'type')
            ->type('This is my test page', 'content')
            ->select('frontend.layouts.master', 'layout')
            ->select('1', 'active')
            ->press('Save');

        $this->seeInDatabase('pages',
            [
                'name' => 'Test Page',
                'uri' => 'test-page',
                'content' => 'This is my test page',
                'layout' => 'frontend.layouts.master',
                'active' => 1
            ]);

        $this->visit('test-page')->see('This is my test page')->seePageIs('test-page');
    }


    protected function create_user($admin = true)
    {
        $user = factory(App\User::class)->create();

        if ($admin) {

            $role = \Pta\Guardian\Models\Role::where('name', 'admin')->first();
            $user->roles()->attach($role);
        }

        return $user;
    }
}
