<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminUserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * AdminUserTest constructor.
     */
    public function __construct()
    {

    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_can_create_admin_role()
    {
        $user = factory(App\User::class)->create();

        $role = \Pta\Guardian\Models\Role::where('name', 'admin')->first();

        $user->roles()->attach($role);

        $this->seeInDatabase('users', ['username' => $user->username, 'email' => $user->email]);

        $this->seeInDatabase('role_user', ['user_id' => $user->id, 'role_id' => 1]);
    }

    public function test_admin_user_can_access_admin_panel()
    {
        $user = $this->create_admin_user();

        \Auth::loginUsingId($user->id);

        $admin_path = $this->getAdminPath();

        $this->visit($admin_path)->see('Hello world');
    }

    public function test_logged_in_user_without_admin_role_cant_access_admin_panel()
    {
        $user = factory(App\User::class)->create();

        \Auth::loginUsingId($user->id);

        $admin_path = $this->getAdminPath();

        $redirect_path = config('core.redirect_url');

        $this->visit($admin_path)->seePageIs($redirect_path);
    }

    public function test_user_cant_access_admin_panel()
    {
        $admin_path = $this->getAdminPath();

        $redirect_path = config('core.redirect_url');

        $this->visit($admin_path)->seePageIs($redirect_path);
    }


    protected function create_admin_user()
    {
        $user = factory(App\User::class)->create();

        $role = \Pta\Guardian\Models\Role::where('name', 'admin')->first();

        $user->roles()->attach($role);

        return $user;
    }

    protected function getAdminPath()
    {
        return config('core.admin_uri') ?: '/dashboard';
    }

}
