<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserRegisterTest extends TestCase
{
    use DatabaseTransactions;

    public function test_user_can_reach_register_page()
    {
        $url = route('auth::register');

        $this->visit($url)->see('Register');

        $this->seePageIs($url);
    }

    public function test_user_can_register()
    {

        $url = route('auth::register');

        $this->visit($url)
            ->type('Username', 'username')
            ->type('user@email.com', 'email')
            ->type('password1', 'password')
            ->type('password1', 'password_confirmation')
            ->press('Register');

        $this->seeInDatabase('users', ['username' => 'Username', 'email' => 'user@email.com']);
    }

    public function test_user_cant_register_same_username()
    {
        $url = route('auth::register');

        $user = factory(\App\User::class)->create();

        $this->visit($url)
            ->type($user->username, 'username')
            ->type($user->email, 'email')
            ->type('password1', 'password')
            ->type('password1', 'password_confirmation')
            ->press('Register');

        $this->see('The username has already been taken.');
    }


    public function test_user_cant_register_same_email()
    {
        $url = route('auth::register');

        $user = factory(\App\User::class)->create();

        $this->visit($url)
            ->type($user->username, 'username')
            ->type($user->email, 'email')
            ->type('password1', 'password')
            ->type('password1', 'password_confirmation')
            ->press('Register');

        $this->seePageIs($url);

        $this->see('The email has already been taken.');

    }

    public function test_user_must_confirm_password()
    {
        $url = route('auth::register');

        $this->visit($url)
            ->type('Username', 'username')
            ->type('user@email.com', 'email')
            ->type('password1', 'password')
            ->press('Register');

        $this->seePageIs($url);

        $this->see('The password confirmation does not match.');

    }

    public function test_user_must_provide_six_character_password()
    {
        $url = route('auth::register');

        $this->visit($url)
            ->type('Username', 'username')
            ->type('user@email.com', 'email')
            ->type('pas', 'password')
            ->type('pas', 'password_confirmation')
            ->press('Register');

        $this->seePageIs($url);

        $this->see('The password must be at least 6 characters.');

    }
}
