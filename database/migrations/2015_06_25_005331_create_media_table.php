<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('user_id')->unsigned();
			$table->string('mime_type')->nullable();
			$table->string('path');
			$table->integer('file_size')->default('0')->unsigned();
			$table->integer('width')->default('0')->unsigned();
			$table->integer('height')->default('0')->unsigned();
			$table->integer('private')->default('0')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media');
	}

}
