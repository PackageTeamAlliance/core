<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCropsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media_crops', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('media_id')->unsigned();
			$table->string('type');
			$table->string('width');
			$table->string('height');
			$table->string('constrain');
			$table->string('ext');
			$table->string('path');
			$table->string('mime_type')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media_crops');
	}

}
